print "////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"
print "								Bienvenido al juego Battleship!													   						"
print "		1 Es una version simplificada para un jugador del clasico juego de mesa que ya conoces 							   				"
print "		2 En esta version del juego, habra un solo buque escondido en una ubicacion aleatoria en una cuadricula de 5x5.		   			"
print "		3 Tendras 5 oportunidades para intentar hundir el barco, solo necesitamos que nos des las coordenadas y hundiremos el buque.	"
print "////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////"


from random import randint

board = []
print ""
print "Este es su tablero de juego"
print ""
for i in range(0,5):
  board.append(["O"] * 5)

def print_board(board):
	for row in board:
		print " ".join(row)
print_board(board)
print ""
def random_row(board):
	return randint(0, len(board[0])  -1)

def random_col(board):
	return randint(0, len(board[0])  -1)

ship_row = random_row(board)
ship_col = random_col(board)

nombre = raw_input("Introduzca su nombre por favor: ").capitalize()
while not nombre.isalpha():
	print ""
	print "ALERTA: Debe ingresar un nombre."
	print ""
	nombre = raw_input("Introduzca su nombre por favor: ")
			
print ""
print "Bienvenido a Battleship Capitan %s" % nombre
print "Existe un bueque de guerra escondido en alguna parte del oceano"
print "Necesitamos nos diga cuales son las coordenadas para ubicarlo"
print ""
print "Tu mision: Hundir el buque"
print ""
print "Para lograrlo necesitamos nos diga cuales son las coordenadas del buque"
for turn in range(6):
	print "			======="
	print "			Turno", turn + 1
	print "			======="

	while True:
		try:
			guess_row = int(raw_input("Diga la longitud(fila): "))
			guess_col = int(raw_input("Diga la latitud(columna): "))
			break
		except ValueError:
			print ""
			print "ALERTA: Debe ingresar un valor numerico."
			guess_col = int(raw_input("Diga la latitud(columna): "))			
			break
		except ValueError:
			print ""
			print "ALERTA: has ingresado una letra, porfavor introduce un numero."
			print ""
	if guess_row == ship_row and guess_col == ship_col:
		print ""
		print "Happyliteichon! Hundimos el buque de guerra!"
		print "Excelente trabajo! Capitan %s" % nombre
		board[guess_row][guess_col] = "*"
		print_board(board)
		break
	else:
		print ""
		print "Rayos!, has perdido de vista el buque de guerra."
	if turn +1 == 5:
		print "Se te agotaron las oportunidades, el buque se ha ido y no lograstes hundirlo"
		print ""
		print "			:("
		print ""
		print "Estas eran las coordenadas donde se ubicaba el buque:"
		print ""
		print "Longitud (fila) %s" % ship_row
		print "Latitud (columna) %s" % ship_col
		break
	if guess_row<0 or guess_row>(len(board)-1) or guess_col<0 or guess_col>(len(board)-1):
  		print "Nos has dado unas coordenadas que no estan en esta parte del oceano, vuelve a intentarlo"

	elif guess_row == ship_row and guess_col != ship_col or guess_row != ship_row and guess_col == ship_col:
		print "Vamos intentalo de nuevo casi adivinas"
		board[guess_row][guess_col] = "X"
		print ""
	else:
		board[guess_row][guess_col] = "X"
		print ""
	print_board(board)