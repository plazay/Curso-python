#*****************************************lo aprendido en python avanzado (leccion 10)**************************************************************************************************************
  #-------------------------------------------------------------------------------------------------------------------------------------

my_dict = {
  "Nombre" : "Yoneiker",
  "Edad" : 24,
  "Profesion" : "Programador"
}
print my_dict.items()#----> imprime valores y llaves
print my_dict.keys()#----> solo imprime las llves
print my_dict.values()#----> solo imprime los valores

    #------------------------------------------------------------------------------------------------------------------------------------

evens_to_50 = [i for i in range(51) if i % 2 == 0]
print evens_to_50#-----> imprime los valores de 2 en 2 hasta el 50
    
    #--------------------------------------------------------------------------------------------------------------------

doubles_by_3 = [x * 2 for x in range(1, 6) if (x * 2) % 3 == 0]

even_squares = [x**2 for x in range(1,11) if x**2 % 2 == 0]

print even_squares

cubes_by_four = [n**3 for n in range(1,11) if not n%2]
print cubes_by_four#----> imprime los valores de los cubos de los números del 1 al 10 solo si el cubo es divisible por cuatro.
    
    #-------------------------------------------------------------------------------------------------------------------------------------

l = [i ** 2 for i in range(1, 11)]
# Should be [1, 4, 9, 16, 25, 36, 49, 64, 81, 100]

print l[2:9:2]#----> imprime 9, 49, 81
    
    #-------------------------------------------------------------------------------------------------------------------------------------

my_list = range(1, 11) # List of numbers 1 - 10

# Add your code below!
print my_list[::2]#----> imprimir numeros impares omitiendo el primer y segundo numero

    #-------------------------------------------------------------------------------------------------------------------------------------

my_list = range(1, 11)

# Add your code below!
backwards = my_list[::-1]
print backwards #-----> imprime los numeros al reves

    #-------------------------------------------------------------------------------------------------------------------------------------

to_one_hundred = range(101)
# Add your code below!
backwards_by_tens = to_one_hundred[::-10]
print backwards_by_tens#-----> conteo regresivo de 10 en 10

    #-------------------------------------------------------------------------------------------------------------------------------------

languages = ["HTML", "JavaScript", "Python", "Ruby"]
my_list = range(16)
print filter(lambda x: len(x) == 6,languages)# si el numero de caracteres es igual a 6 imprimelo
print filter(lambda x:x=="Python" , languages)# tambien podemos usar esta forma de la funcion hace lo mismo que la anterior

    #-------------------------------------------------------------------------------------------------------------------------------------

squares = [i**2 for i in range(1,11)]
print filter(lambda x: x in range(30,71), squares)

    #-----------------------------------------------------------------------------------------------------------------------------------------

threes_and_fives=[x for x in range(1,16) if x%3==0 or x%5==0]

    #-----------------------------------------List Slicing-------------------------------------------------------------------------------------

garbled = "!XeXgXaXsXsXeXmX XtXeXrXcXeXsX XeXhXtX XmXaX XI"
message = garbled[::-2]
print message

    #-----------------------------------------Lambda Expressions-------------------------------------------------------------------------------------

garbled = "IXXX aXXmX aXXXnXoXXXXXtXhXeXXXXrX sXXXXeXcXXXrXeXt mXXeXsXXXsXaXXXXXXgXeX!XX"
message = filter(lambda x: x !="X", garbled)
print message

#/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



    #-----------------------------------------bit bitwise-------------------------------------------------------------------------------------

print 5 >> 4  # Right Shift
print 5 << 1  # Left Shift
print 8 & 5   # Bitwise AND
print 9 | 4   # Bitwise OR
print 12 ^ 42 # Bitwise XOR
print ~88     # Bitwise NOT

    #-----------------------------------------Lesson I0: The Base 2 Number System-------------------------------------------------------------

print 0b1,    #1
print 0b10,   #2
print 0b11,   #3
print 0b100,  #4
print 0b101,  #5
print 0b110,  #6
print 0b111   #7
print "******"
print 0b1 + 0b11
print 0b11 * 0b11

    #-------------------------------------------------------------------------------------------------------------------------------------------------

one = 0b1
two = 0b10
three = 0b11
four = 0b100 
five = 0b101
six = 0b110
seven = 0b111
eight = 0b1000
nine = 0b1001
ten = 0b1010
eleven = 0b1011
twelve = 0b1100

    #------------------------------------------imprimir codigo de numeros binarios-------------------------------------------------------

print bin(1)
print bin(2)
print bin(3)
print bin(4)
print bin(5)

    #-------------------------------------------------------------------------------------------------------------

print int("1",2)            #1
print int("10",2)           #2
print int("111",2)          #7
print int("0b100",2)        #4
print int(bin(5),2)         #5
print int("0b11001001",2)   #201

    #--------------------------------------------------------------------------------------------------------------

shift_right = 0b1100
shift_left = 0b1

shift_right = shift_right >> 2
shift_left = shift_left << 2
print bin(shift_right)
print bin(shift_left)

    #----------------------------------------Operadores bit----------------------------------------------------------

print bin(0b1110 & 0b101)# = 0b100(4)
print bin(0b1110 | 0b101)# = 0b1111(15)
print bin(0b1110 ^ 0b101)# = 0b1011(11)
print ~1# -2------------
print ~2# -3            |
print ~3# -4            |-----> al parecer este operador le suma un numero y lo coloca negativo
print ~42# -43          |
print ~123# -124--------

    #-------------------------------------------------------------------------------------------------------------

def check_bit4(input):
    if input & 0b1000 > 0:
        return "on"
    else:
        return "off"

    #-------------------------------------------------------------------------------------------------------------

a = 0b10111011
bitmask = 0b100
print bin(a | bitmask)# 0b10111111(191)

    #-------------------------------------------Ejercicio para entender--------------------------------------------------------------

a = 0b11101110
mask = 0b11111111
flip = a^mask
print bin(flip)
    
    #-------------------------------------------Ejercicio para entender--------------------------------------------------------------   

def flip_bit(number, n):
    mask = 0b1 << n-1
    desired = number ^ mask
    return bin(desired)
#*****************************************************************************************************************************************************************************************




#*****************************************lo aprendido en python avanzado (leccion 11)**************************************************************************************************************
    
    #------------------------------------ejemplo de clases--------------------------------------------------------  

class Fruit(object):
  """Una clase que produce varias frutas sabrosas."""
  def __init__(self, name, color, flavor, poisonous):
    self.name = name
    self.color = color
    self.flavor = flavor
    self.poisonous = poisonous

  def description(self):
    print "I'm a %s %s and I taste %s." % (self.color, self.name, self.flavor)

  def is_edible(self):
    if not self.poisonous:
      print "Yep! I'm edible."
    else:
      print "Don't eat me! I am super poisonous."

lemon = Fruit("lemon", "yellow", "sour", False)

lemon.description()
lemon.is_edible()

    #--------------------------------------------Ejemplo de definicion de clases---------------------------------------------------------------

# Class definition
class Animal(object):
  """Makes cute animals."""
  # For initializing our instance objects
  def __init__(self, name, age, is_hungry):
    self.name = name
    self.age = age
    self.is_hungry = is_hungry

# Note that self is only used in the __init__()
# function definition; we don't need to pass it
# to our instance objects.

zebra = Animal("Jeffrey", 2, True)
giraffe = Animal("Bruce", 1, False)
panda = Animal("Chad", 7, True)

print zebra.name, zebra.age, zebra.is_hungry
print giraffe.name, giraffe.age, giraffe.is_hungry
print panda.name, panda.age, panda.is_hungry
    
    #----------------------------------------------------Metodos en las clases-----------------------------------------------

class Animal(object):
  """Makes cute animals."""
  is_alive = True
  def __init__(self, name, age):#---------->constructor que inicializa las variables
    self.name = name
    self.age = age
  def description(self):
      print self.name
      print self.age

hippo = Animal("sdfg", "25")#---
hippo.description()         #   |--------------> Aqui le estoy enviando los datos que van a la variable antes inicializada
fox = Animal("diego", "15")#----
fox.description()

    #----------------------------------------------------Agregar items-----------------------------------------------

class ShoppingCart(object):
  """Creates shopping cart objects
  for users of our fine website."""
  items_in_cart = {}
  def __init__(self, customer_name):
    self.customer_name = customer_name

  def add_item(self, product, price):
    """Add product to the cart."""
    if not product in self.items_in_cart:
      self.items_in_cart[product] = price
      print product + " added."
    else:
      print product + " is already in the cart."

  def remove_item(self, product):
    """Remove product from the cart."""
    if product in self.items_in_cart:
      del self.items_in_cart[product]
      print product + " removed."
    else:
      print product + " is not in the cart."   
my_cart = ShoppingCart("Yone")#-------\
my_cart.add_item("Table", 20)#--------/

    #----------------------------------------------------Herencias (inheritance) de atributos de otra clase-----------------------------------------------

class Customer(object):
  """Produces objects that represent customers."""
  def __init__(self, customer_id):
    self.customer_id = customer_id

  def display_cart(self):
    print "I'm a string that stands in for the contents of your shopping cart!"

class ReturningCustomer(Customer):
  """For customers of the repeat variety."""
  def display_order_history(self):
    print "I'm a string that stands in for your order history!"

monty_python = ReturningCustomer("ID: 12345")
monty_python.display_cart()
monty_python.display_order_history()
    
    #---------------------------------------------------Sintaxis de inheritance-----------------------------------

class Shape(object):
  """Makes shapes!"""
  def __init__(self, number_of_sides):
    self.number_of_sides = number_of_sides

# Add your Triangle class below!
class Triangle(Shape): #clase Triangle sera la nueva clase y Shape la clase de la que hereda esa nueva clase.
  def __init__ (self, side1, side2, side3):
        self.side1 = side1
        self.side2 = side2
        self.side3 = side3  

    #--------------------------------------------------Anular o sobreescribir métodos y atributos de la clases padre-----------------------------------------------------------

class Employee(object):
  """Models real-life employees!"""
  def __init__(self, employee_name):
    self.employee_name = employee_name

  def calculate_wage(self, hours):
    self.hours = hours
    return hours * 20.00

# Add your code below!
class PartTimeEmployee(Employee):
    def calculate_wage(self, hours):
        self.hours = hours
        return hours * 12.00
    
    #--------------------------------------------------super llamada incorporada de python----------------------------------------------------------

class Employee(object):
  """Models real-life employees!"""
  def __init__(self, employee_name):
    self.employee_name = employee_name

  def calculate_wage(self, hours):
    self.hours = hours
    return hours * 20.00

# Add your code below!
class PartTimeEmployee(Employee):
        def calculate_wage(self,hours):
            self.hours = hours 
            return hours * 12.00
        def full_time_wage(self,others):
            return         super(PartTimeEmployee,self).calculate_wage(others)


milton = PartTimeEmployee("milton")
print milton.full_time_wage(10)
    
    #--------------------------------------------------Ejercicio de prueba----------------------------------------------------------

class Triangle(object):
    def __init__(self, angle1, angle2, angle3):
        self.angle1 = angle1
        self.angle2 = angle2
        self.angle3 = angle3
    number_of_sides = 3
    def check_angles(self):
        if (self.angle1 + self.angle2 + self.angle3 == 180):
            return True
        else:
            return False
my_triangle = Triangle(90, 30, 60)
print my_triangle.number_of_sides
print my_triangle.check_angles()

class Equilateral(Triangle):
    angle = 60
    def __init__(self):
        self.angle1 = self.angle
        self.angle2 = self.angle
        self.angle3 = self.angle
    
    #-----------------------------------------Ejercicio final de clases------------------------------------------------------------------

class Car(object):
  condition = "new"
  def __init__(self, model, color, mpg):
    self.model = model
    self.color = color
    self.mpg   = mpg
  
  def display_car(self):
    return str("This is a " + self.color + " " + self.model + " with " + str(self.mpg) + " MPG.")
 
  def drive_car(self):
    self.condition = "used"
    return self.condition
        
my_car = Car("DeLorean", "silver", 88)
print my_car.condition
print my_car.drive_car()

class ElectricCar(Car):
  def __init__(self, model, color, mpg, battery_type):
    self.battery_type = battery_type
    self.model = model
    self.color = color
    self.mpg   = mpg
  def drive_car(self):
    self.condition = "like new"
    return self.condition
my_car = ElectricCar("Hyundai", "Gray", 90, "molten salt")
print my_car.drive_car()
  
  #-----------------------------------------Representar un objeto __repr__------------------------------------------------------------------

class Point3D(object):
  def __init__(self, x, y, z):
    self.x = x
    self.y = y
    self.z = z
  def __repr__(self):
    return str("(%d, %d, %d)" % (self.x, self.y, self.z))
my_point = Point3D(1, 2, 3)
print my_point
#************************************************************************************************************************************************************************************************





#******************************************lo aprendido en python avanzado (leccion 12)***********************************************************************************************************************************************

  #----------------------------------escribir el contenido de my_list en un archivo de texto llamado output.txt.-----------------------------------------------------------------

my_list = [i ** 2 for i in range(1, 11)]
# Generates a list of squares of the numbers 1 - 10

f = open("output.txt", "w")

for item in my_list:
  f.write(str(item) + "\n")

f.close()
  
  #----------------------------------leer el contenido de my_file en un archivo de texto llamado output.txt.-----------------------------------------------------------------

my_file=1
my_file= open('output.txt', 'r')
print my_file.read()
my_file.close()
  
  #----------------------------------leer el contenido de my_file en un archivo de texto llamado text.txt linea por linea.-----------------------------------------------------------------

my_file = open('text.txt', 'r')
print my_file.readline()
print my_file.readline()
print my_file.readline()
my_file.close()
  
  #----------------------------------leer el contenido de my_file en un archivo de texto llamado text.txt linea por linea.-----------------------------------------------------------------

"""datos de almacenamiento en búfer
Seguimos diciéndole que siempre debe cerrar sus archivos después de que haya terminado de escribirles. ¡Este es el por qué!

Durante el proceso de E / S, los datos se almacenan en búfer : 
esto significa que se guardan en una ubicación temporal antes de escribirse en el archivo.

Python no vacía el búfer , es decir, escribe datos en el archivo, hasta que esté seguro de que ha terminado de escribir. 
Una forma de hacerlo es cerrar el archivo. Si escribe en un archivo sin cerrar, 
los datos no llegarán al archivo de destino."""



# Use a file handler to open a file for writing
write_file = open("text.txt", "w")

# Open the file for reading
read_file = open("text.txt", "r")

# Write to the file
write_file.write("Not closing files is VERY BAD.")
write_file.close()

read_file.close()
# Try to read from the file
print read_file.read()
  
  #----------------------------------Agrega la palabra "Success! al archivo text.txt".-----------------------------------------------------------------

with open("text.txt", "w") as textfile:
  textfile.write("Success!")
  
  #----------------------------------Para chequear si esta cerrado el archivo.-----------------------------------------------------------------

with open("text.txt", "w") as my_file:
    my_file.write('hello world')
if my_file.close():
    my_file.closed
print my_file.closed
#************************************************************************************************************************************************************************************************