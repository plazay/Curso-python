#*****************************************lo aprendido en la septima leccion**************************************************************************************************************

    #----------------------------------------------PARA TENER EN CUENTA---------------------------------------------------------------------------------------------------------

n = [1, 3, 5]

print n[1]

n = [1, 3, 5]
# Do your multiplication here
n[1] = n[1] * 5
print n


n = [1, 3, 5]
# Append the number 4 here
n.append(4) 
print n

n = [1, 3, 5]
# Remove the first item in the list here
n.pop(0)
print n

n.remove(1)
# Removes 1 from the list,
# NOT the item at index 1
print n
# prints [3, 5]

del(n[1])
# Doesn't return anything
print n
# prints [1, 5]

number = 5

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def my_function(x):
    return x * 3

print my_function(number)

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

m = 5
n = 13

def add_function(x, y):
    return x + y
print add_function(m, n)



m = [1, 2, 3]
n = [4, 5, 6]


def join_lists(x, y):
    return x + y
print join_lists(m, n)# You want this to print [1, 2, 3, 4, 5, 6]

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

n = "Hello"
def string_function(s):
    return s + 'world'

print string_function(n)# se imprime concatenado

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#el siguiente ejercicio pasa una lista a una funcion
def list_function(x):
    return x

n = [3, 5, 7]
print list_function(n)

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def list_function(x):
    return x[1]

n = [3, 5, 7]
print list_function(n)

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def list_function(x):
    x[1] = x[1] + 3
    return x

    n = [3, 5, 7]
print list_function(n)

    #--------------------------------------Impresión de una posición de lista por posición en una función-----------------------------------------------------------------------------------------------------------------------------------------------

n = [3, 5, 7]
def print_list(x):
    for i in range(0, len(x)):
        print x[i]
print print_list(n)

    #--------------------------------------Modificación de cada elemento de una lista en una función-----------------------------------------------------------------------------------------------------------------------

n = [3, 5, 7]

def double_list(x):
    for i in range(0, len(x)):
        x[i] = x[i] * 2
    return x

print double_list(n)

    #---------------------------------------Pasar un rango a una función-----------------------------------------------------------------------------------------------

def my_function(x):
    for i in range(0, len(x)):
        x[i] = x[i]
    return x

print my_function(range(3))#---> range(stop), range(start, stop), range(start, stop, step)

    #----------------------Función que devuelva la suma de una lista de números (Iterating over a list in a function)----------------------------------------------------------------------------------------------------------------------------------------------

n = [3, 5, 7]

def total(numbers):
    result = 0
    for i in range(len(numbers)):
        result += numbers[i]
    return result

    #---------------------------------------Función que concatene string.----------------------------------------------------------------------------------------------------------------------------------------------

n = ["Michael", "Lieberman"]
# Add your function here
def join_strings(words):
    result=""
    for i in words:
        result +=i
    return result

print join_strings(n)#---> imprime MichaelLieberman

    #----------------------------Utilización de una lista de listas en una función---------------------------------------------------------------------------------------------------------------------------------------------------------

n = [[1, 2, 3], [4, 5, 6, 7, 8, 9]]
# Add your function here
def flatten (lists):
    result = []
    for i in range(len(lists)):
        for j in range(len(lists[i])):
            result.append(lists[i][j])
    return result
print flatten(n)# imprime lo siguiente [1, 2, 3, 4, 5, 6, 7, 8, 9]

#*****************************************************************************************************************************************************************************************




#*****************************************lo aprendido en la octava leccion**************************************************************************************************************
    #--------------------------------------------------while-----------------------------------------------------------------------------------------------------------------------------------

count = 0

if count < 10:
    print "Hello, I am an if statement and count is", count

while count < 10:
    print "Hello, I am a while and count is", count
count += 1

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

loop_condition = True

while loop_condition:
    print "I am a loop"
    loop_condition = False

    #---------------------------------------------------while----------------------------------------------------------------------------------------------------------------------------------

num = 1

while num <= 10:
    print num**2
    num += 1

    #----------------------------------------------------break---------------------------------------------------------------------------------------------------------------------------------

choice = raw_input('Enjoying the course? (y/n)')

while choice != "y" and choice != "n":  
    choice = raw_input("Sorry, I didn't catch that. Enter again: ")

    #---------------------------------------------------while / else----------------------------------------------------------------------------------------------------------------------------------

import random

print "Lucky Numbers! 3 numbers will be generated."
print "If one of them is a '5', you lose!"

count = 0
while count < 3:
    num = random.randint(1, 6)
    print num
    if num == 5:
        print "Sorry, you lose!"
        break
    count += 1
else:
    print "You win!"

    #---------------------------------------------Your own while / else----------------------------------------------------------------------------------------------------------------------------------------

from random import randrange

random_number = randrange(1, 10)

guesses_left = 0
while guesses_left < 3:
    guess = int(raw_input("Enter a guess:"))
    guesses_left -= 1
    if guess == random_number:
        print "You win!"
        break
else:
    print "You lose"

    #----------------------------------------------------For your strings---------------------------------------------------------------------------------------------------------------------------------

thing = "spam!"

for c in thing:
    print c

word = "eggs!"

for c in word:
    print c

    #-----------------------------------------------------For your A--------------------------------------------------------------------------------------------------------------------------------

phrase = "A bird in the hand..."

# Add your for loop
for char in range(len(phrase)):
    if phrase[char] == "A" or phrase[char] == "a":
        print "X",
    else: print phrase[char],

#Don't delete this print statement!
print

    #-----------------------------------------------------For your lists------------------------------------------------------------------------------------------------------------------------------

numbers  = [7, 9, 12, 54, 99]

print "This list contains: "

for num in numbers:
    print num

# Add your loop below!
for num in numbers:
    print num ** 2

    #-----------------------------------------------------Looping over a dictionary-------------------------------------------------------------------------------------------------------------------------------

d = {'a': 'apple', 'b': 'berry', 'c': 'cherry'}

for key in sorted(d):
    print key, d[key]

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

choices = ['pizza', 'pasta', 'salad', 'nachos']

print 'Your choices are:'
for index, item in enumerate(choices):
    print index +1, item

    #-----------------------------------------------------Multiple list mostrar el valor mas alto-------------------------------------------------------------------------------------------------------------------------------

list_a = [3, 9, 17, 15, 19]
list_b = [2, 4, 8, 10, 30, 40, 50, 60, 70, 80, 90]

for a, b in zip(list_a, list_b):
    print max(a, b)

    #------------------------------------------------------Frenar el ciclo con una palabra-------------------------------------------------------------------------------------------------------------------------------

fruits = ['banana', 'apple', 'orange', 'tomato', 'pear', 'grape']

print 'You have...'
for f in fruits:
    if f == 'tomato':
        print 'A tomato is not a fruit!' # (It actually is.)
    break
    print 'A', f
else:
    print 'A fine selection of fruits!'

    #----------------------------------------------------No se frena en tomate si no en banana---------------------------------------------------------------------------------------------------------------------------------

fruits = ['banana', 'apple', 'orange', 'tomato', 'pear', 'grape']

print 'You have...'
for f in fruits:
    if f == 'tomato':
        print 'A tomato is not a fruit!' # (It actually is.)
else:
        print 'A fine selection of fruits!'
    print 'A', f
    break

    #----------------------------------------------------is_int---------------------------------------------------------------------------------------------------------------------------------

def is_int(x):
    if x-round(x) == 0: 
        return True
    else: 
        return False

    #----------------------------------------------------Suma de digitos--------------------------------------------------------------------------------------------------------------------------------

def digit_sum(x):
        string_x = str(x)
    total = 0
    for char in string_x:
        total += int(char)
    return total

    #-----------------------------------------------------Factorial--------------------------------------------------------------------------------------------------------------------------------

def factorial(x):
    if x == 1:
        return x
    else:
        return x * factorial(x-1)

    #-------------------------------------------------------is prime------------------------------------------------------------------------------------------------------------------------------

def is_prime(x):
    if x < 2:
        return False
    else:
        for n in range(2,x):
            if x % n == 0:
            return False
        return True

    #--------------------------------------------------------   Imprime la palabra al reves----------------------------------------------------------------------------------------------------------------------------

def reverse(s):
    c = len(s)
    t = ''
    while c > 0:
        c -= 1
        t += s[c]
    return t

print reverse('Python!')

    #-------------------------------------------------------Elimina todas las vocales de la palabra------------------------------------------------------------------------------------------------------------------------------

def anti_vowel(text):
    new = []
    vowels = "aeiou"
    for i in text:
        if not(i.lower() in vowels):
            new.append(i)
    return "".join(new)

    #-----------------------------------------------------Scrabble score--------------------------------------------------------------------------------------------------------------------------------

score = {"a": 1, "c": 3, "b": 3, "e": 1, "d": 2, "g": 2, 
 "f": 4, "i": 1, "h": 4, "k": 5, "j": 8, "m": 3, 
 "l": 1, "o": 1, "n": 1, "q": 10, "p": 3, "s": 1, 
 "r": 1, "u": 1, "t": 1, "w": 4, "v": 4, "y": 4, 
 "x": 8, "z": 10}

def scrabble_score(word):
    word = word.lower()
    total=0
    for char in word:
        total = total + (score[char])
    return total

    #----------------------------------------------------------Censor-------------------------------------------------------------------------------------------------------------------------------

def censor(text,word):
    return " ".join(["*"*len(w) if w in word else w for w in text.split()])

    #----------------------------------------------------------Otro ejemplo de count---------------------------------------------------------------------------------------------------------------------------

def count(seq, item):
    total = 0
    for n in seq:
        if n == item:   
            total = total +1
    return total

    #----------------------------------------------------------Purify-------------------------------------------------------------------------------------------------------------------------------

def purify(l):
    n = []
    for i in l:
        if i % 2 == 0: n.append(i)
    return n

    #----------------------------------------------------------Product---------------------------------------------------------------------------------------------------------------------------

def product(lst):
    total = 1
    for i in lst:
        total *= i
    return total

    #----------------------------------------------------------remove duplicates---------------------------------------------------------------------------------------------------------------------------

def remove_duplicates(lst):
    result = []
    for i in lst:
        if i not in result:
        result.append(i)
    return result

    #----------------------------------------------------------Ejercicio para practicar y entender---------------------------------------------------------------------------------------------------------------------------

print remove_duplicates([1,3,1,2,1,2])

"""promedio
Escribamos una función para encontrar la mediana de una lista.

La mediana es el número medio en una secuencia ordenada de números.

Encontrar la mediana de[7,12,3,1,6] consistiría primero en ordenar la secuencia en[1,3,6,7,12] 
y luego ubicar el valor medio, que sería 6. La mediana de la mediana de[7,12,3,1,6] consiste en ordenar la secuencia en[1,3,6,7,12] 
y luego ubicar el valor medio, que sería 6.

Si se le da una secuencia con un número par de elementos, debe promediar los dos elementos que rodean el centro.

Por ejemplo, la mediana de la secuencia[7,3,1,4] es 3,5, ya que los elementos intermedios después de ordenar 
la lista son 3 y 4 y (3 + 4) / (2,0) es 3,5. La mediana de la secuencia[7,3,1,4] es 3,5, 
ya que los elementos intermedios después de ordenar la lista son 3 y 4 y (3 + 4) / (2,0).

Puede clasificar la secuencia utilizando la función sorted (), así: sorted([5, 2, 3, 1, 4]) [1, 2, 3, 4, 5]"""
        
    """Write a function called median that takes a list as an input and returns the median value of the list. 
    For example: median([1, 1, 2]) should return 1.

    The list can be of any size and the numbers are not guaranteed to be in any particular order. Make sure to sort it!
    If the list contains an even number of elements, your function should return the average of the middle two."""

        def median(lst):
            s = sorted(lst)
            l = len(lst)/2
            if len(lst)%2 == 0:
                return (s[l] + s[l-1])/2.0
            else:
                return s[l]

    #----------------------------------------------------------Ejercicio para practicar y entender---------------------------------------------------------------------------------------------------------------------------

#*****************************************************************************************************************************************************************************************




#*****************************************lo aprendido en la novena leccion**************************************************************************************************************

    #-----------------------------------------------------Suma de las puntuaciones------------------------------------------------------------------------------------------------------------

grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def grades_sum(scores):
    total = 0
    for score in scores:
        total += score
    print total
    return total

grades_sum(grades)

    #------------------------------------------------------Para entender y practicar-------------------------------------------------------------------------------------------------------------------------------

grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def print_grades(x):
    for i in range(len(x)):
        print x[i]

print print_grades(grades)

    #------------------------------------------------------Para entender y practicar---------------------------------------------------------------------------------------------------------------

grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def grades_sum(scores):
    total_score = 0
    for score in scores:
        total_score += score
    return total_score

def grades_average(grades):
    average = grades_sum(grades) / float(len(grades))
    return average

print "Total Score", grades_sum(grades)
print "Average Score", grades_average(grades)

    #------------------------------------------------------------------------------------------------------------------------------------

"""Vamos a usar el promedio para calcular la varianza(desviacion). 
La varianza(desviacion) nos permite ver cuán extendidas fueron las calificaciones del promedio."""

grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def print_grades(grades_input):
  for grade in grades_input:
    print grade

def grades_sum(scores):
  total = 0
  for score in scores: 
    total += score
  return total
    
def grades_average(grades_input):
  sum_of_grades = grades_sum(grades_input)
  average = sum_of_grades / float(len(grades_input))
  return average

def grades_variance(x):
    average = grades_average(x)
    variance = 0
    for i in x:
        variance += (average - i) ** 2
    result = variance / len(x)
    return result
print grades_variance(grades)#------> muestra este valor 334.071005917

    #---------------------------------------------------------Desviacion estandar-----------------------------------------------------------------------------------------------------------------------------

"""La desviación estándar es la raíz cuadrada de la varianza. calculamos la raíz cuadrada aumentando el número a la mitad de potencia."""

grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def print_grades(grades_input):
  for grade in grades_input:
    print grade

def grades_sum(scores):
  total = 0
  for score in scores: 
    total += score
  return total
    
def grades_average(grades_input):
  sum_of_grades = grades_sum(grades_input)
  average = sum_of_grades / float(len(grades_input))
  return average

def grades_variance(x):
    average = grades_average(x)
    variance = 0
    for i in x:
        variance += (average - i) ** 2
    result = variance / len(x)
    return result
print grades_variance(grades)

def grades_std_deviation(variance):#--------------------|
    return variance ** 0.5#                             |fragmento de la
    variance = grades_variance(grades)#                 |desviacion estandar
    print grades_std_deviation(variance)#---------------|

    #-----------------------------------------------------Ejercicio de estadisticas completo------------------------------------------------------------------------------

grades = [100, 100, 90, 40, 80, 100, 85, 70, 90, 65, 90, 85, 50.5]

def print_grades(grades_input):
  for grade in grades_input:
    print grade

def grades_sum(scores):
  total = 0
  for score in scores: 
    total += score
  return total
    
def grades_average(grades_input):
  sum_of_grades = grades_sum(grades_input)
  average = sum_of_grades / float(len(grades_input))
  return average

def grades_variance(x):
    average = grades_average(x)
    variance = 0
    for i in x:
        variance += (average - i) ** 2
    result = variance / len(x)
    return result
print grades_variance(grades)

def grades_std_deviation(variance):
    return variance ** 0.5
    variance = grades_variance(grades)
    print grades_std_deviation(variance)

print "Verifique cuidadosamente los resultados obtenidos"
print ""  
print "Estas son todas las notas:--------> %s" % print_grades(grades)
print "Suma de todas las notas:--------> %s" % grades_sum(grades)
print "Promedio de notas:--------> %s" % grades_average(grades)
print "Varianza de las notas:--------> %s" % grades_variance(grades)
print "Desviacion estandar:--------> %s" % grades_std_deviation(grades_variance(grades))

    #-----------------------------------------------------------------------------------------------------------------------------------------
#************************************************************************************************************************************************************************************************