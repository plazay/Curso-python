#*****************************************lo aprendido en la primera leccion**************************************************************************************************************
	

	#------------------------------------------------Codigo mal escrito a proposito-------------------------------------------------------------------------------------------------------

		def spam():
		eggs = 12
		return eggs
        
		print spam()

		"""IndentationError: expected an indented block
		este es el motivo del error de arriba "Obtendrás este error siempre que tu espacio en blanco esté apagado."
		Faltan los espacios de eggs y return"""
	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		10 ** 2 = 100 #ya que ** significa la expresion elevada de la matematica
		3 % 2 = 1 #ya que % significa dividir

	#-----------------------------------------------declaracion de variables--------------------------------------------------------------------------------------------------------------
		monty = True
		python = 1.234
		monty_python = 1.234 ** 2
		print monty_python
	#-----------------------------------------------ejercicio de calcular la propina de una comida----------------------------------------------------------------------------------------
		meal = 44.50
		tax = 6.75 / 100
		tip = 15.0 / 100

		meal = meal + meal * tax
		total = meal + meal * tip

		print("%.2f" % total)
#*****************************************************************************************************************************************************************************************

#*****************************************lo aprendido en la segunda leccion**************************************************************************************************************

	

	#al declarar variables string se debe colocar su valor entre comillas
		#'There's a snake in my boot!'
			#Este código se rompe porque Python cree que el apóstrofe en' There's' termina la cadena. 
			#Podemos usar la barra invertida para arreglar el problema, así:

		#'There\'s a snake in my boot!'
	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


		"""
		The string "PYTHON" tiene seis caracteres,
		numerados del 0 al 5, como se muestra a continuación:

		+---+---+---+---+---+---+
		| P | Y | T | H | O | N |
		+---+---+---+---+---+---+
		  0   1   2   3   4   5

		"""
				
		print "PYTHON"[1] #esto imprimira la varibale 'Y' Del string 'PYTHON'
		print "CASCABEL"[5] #esto imprimiria la variable 'B' del string 'CASCABEL'
	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		parrot = "Norwegian Blue"
		print len(parrot) #imprime 14 que es el numero de caracteres en la frase anterior "Norwegian Blue"
	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		parrot = "Norwegian Blue"
		print parrot.lower() #imprime norwegian blue todo en minuscula ya que omite las letras que esten en mayusculas
	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		parrot = "norwegian blue"
		print parrot.upper() #en caso contrario al ejemplo anterior este imprime toda la frase en mayusculas
	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		pi = 3.14
		print str(pi) # aqui convertimos el tipo de dato que se agrego a string, podemos usar print type(pi) para ver el tipo de dato 
		"""si intentamos imprimir un entero con un numero en el mismo 'print' nos arrojara el siguiente error 
					TypeError: cannot concatenate 'str' and 'float' objects"""

	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		string_1 = "Camelot"
		string_2 = "place"

		print "Let's not go to %s. 'Tis a silly %s." % (string_1, string_2) 
		#el operador '%s' esta llamando las variables declaradas al principio y las muestra en panatalla
	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		name = raw_input("What is your name? ")
		quest = raw_input("What is your quest? ")
		color = raw_input("What is your favorite color? ")

		print "Ah, so your name is %s, your quest is %s, " \
		"and your favorite color is %s." % (name, quest, color)
		#este fragmento de codigo muestra mensajes en pantalla y le asigna a la pregunta el valor introducido por el usuario
	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		from datetime import datetime #<--- esta linea importa el modulo datetime e importa solo datatime.
										# para importar todo del modulo datetime se usa import *


		now = datetime.now() #aqui estamos declarano la varible para que tome la hora actual.
		print now
		print now.year # si queremos imprimir solo el año
		print now.month # imprimir el mes
		print now.day # aqui solo se imprimira el año

		print '%s/%s/%s' % (now.month, now.day, now.year) #para imprimir la fecha en formato deseado
		print '%s:%s:%s' % (now.hour, now.minute, now.second) #tambien se puede escoger el formato de hora deseado	
		print '%s/%s/%s %s:%s:%s' % (now.month, now.day, now.year, now.hour, now.minute, now.second)#formato de hora completo
#*****************************************************************************************************************************************************************************************

#*****************************************lo aprendido en la tercera leccion**************************************************************************************************************
	

	#------------------------------------------------------Ejercicio de practica-----------------------------------------------------------------------------------------------------------

		def clinic():
    		print  "¡Acabas de entrar en la clínica!"
    		print "¿Tomas la puerta on the left or the right?"
    		answer = raw_input("Escriba left or right y presione 'Enter'.").lower()#raw_input() es una funcion para pedir variables al usuario
    		if answer == "left" or answer == "l":
       			print "Esta es la Sala de Abuso Verbal, monton de excrementos de loros!"
    		elif answer == "right" or answer == "r":
        		print "¡Por supuesto que esta es la sala de discusión, ya te lo he dicho!"
    		else:
        		print "¡No elegiste a la izquierda o a la derecha! Inténtalo de nuevo."
        		clinic()

		clinic()
	#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		bool_two = False

		# 1**2 <= -1
		bool_three = False

		# 40 * 4 >= -4
		bool_four = True

		# 100 != 10**2
		bool_five = False
	#--------------------------------------------------Boolean Operators-------------------------------------------------------------------------------------------------------------------
		"""  
		True and True is True
		True and False is False
		False and True is False
		False and False is False

		True or True is True
		True or False is True
		False or True is True
		False or False is False

		Not True is False
		Not False is True

		"""
	#--------------------------------expresiones matematicas que podemos usar en python	----------------------------------------------------------------------------------------------------
	
		False and False   									bool_one = False

		-(-(-(-2))) == -2 and 4 >= 16 ** 0.5        		bool_two = False

		19 % 4 != 300 / 10 / 10 and False					bool_three = False		

		-(1 ** 2) < 2 ** 0 and 10 % 10 <= 20 - 10 * 2		bool_four = True

		True and True										bool_five = True
	#--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		not True								bool_one = False

		not 3 ** 4 < 4 ** 3						bool_two = True

		not 10 % 3 <= 10 % 2					bool_three = True

		not 3 ** 2 + 4 ** 2 != 5 ** 2			bool_four = True

		not not False							bool_five = False
	#-----------------------------------------------------LO ENTENDI COMPLETAMENTE-------------------------------------------------------------------------------
		False or not True and True

		False and not True or True

		True and not (False or False)

		not not True or False and not True

		False or not (True and True)
	#----------------------------------------------------VEAMOS EL SIGUIENTE EJEMPLO------------------------------------------------------------------------------------
	
		# Make me false!
		bool_one = (2 <= 2) and "Alpha" == "Bravo"  # We did this one for you!

		# Make me true!
		bool_two = (5 >= 5) or "Diego" == "diego"

		# Make me false!
		bool_three = not 50 == 5*10 

		# Make me true!
		bool_four = (12 >= 8) or "Diego" == "diego"

		# Make me true!
		bool_five = (58 == 58) and (100 == 10**2)
	#------------------------------------------------------Aqui tenemos otro ejercicio de practica-------------------------------------------------------------------------------------------

		def using_control_once():
    		if 100 == 10**2:
        		return "Success #1"

		def using_control_again():
    		if 588 >= 588:
        		return "Success #2"

		print using_control_once()
		print using_control_again()	
	#------------------------------------------------------EJERCICIO PARA PENSAR Y ENTENDER---------------------------------------------------------------------------------------------------
		

		answer = "'Tis but a scratch!"

		def black_knight():
    		if answer == "'Tis but a scratch!":
        		return True
    		else:
      			print "No es un rasguño"
        		return False  # Make sure this returns False

		def french_soldier():
    		if answer == "Go away, or I shall taunt you a second time!":
        		return True
    		else:
      			print "Te quedaste y me burle"
        		return False      # Make sure this returns False		
	#------------------------------------------------------EJERCICIO PARA PENSAR Y ENTENDER---------------------------------------------------------------------------------------------------
		

		def greater_less_equal_5(answer):
    		if answer > 5:
        		return 1
    		elif answer < 5:          
        		return -1
    		else: 
        		return 0
        
		print greater_less_equal_5(4)
		print greater_less_equal_5(5)
		print greater_less_equal_5(6)
	#------------------------------------------------------EJERCICIO PARA PENSAR Y ENTENDER---------------------------------------------------------------------------------------------------
	
		def the_flying_circus():
    		if answer == "left" or answer == "l":
      			print "Esta es la Sala de Abuso Verbal, monton de excrementos de loros!"
      			return True
    		elif answer == "right" or answer == "r":
      			print "¡Por supuesto que esta es la sala de discusión, ya te lo he dicho!"
      			return True
    		else:
      			print "¡No elegiste a la izquierda o a la derecha! Inténtalo de nuevo."
      			return True
    #------------------------------------------------------Ejercicio de Traduccion pyglatin-----------------------------------------------------------------------------------------------------
		
		"""Pida al usuario que escriba una palabra en inglés.
		Asegúrese de que el usuario haya introducido una palabra válida.
		Convierte la palabra de Inglés a Pig Latin.
		Visualizar el resultado de la conversión."""

			pyg = 'ay'

			original = raw_input('Enter a word:')

			if len(original) > 0 and original.isalpha():
    			print original
			else:
    			print 'empty'
			word = original.lower()
			first = word[0]
			new_word = word + first + pyg #aqui estamos concatenando
			new_word = new_word[1:len(new_word)] #aqui estamos tomando el valor desde el primer indice y colocarlo al final

			print "Traducido al Pyglatin es: "
			print new_word
#************************************************************************************************************************************************************************************************