# -*- encoding: utf-8 -*-

#*****************************************lo aprendido en la cuarta lección**************************************************************************************************************

    #----------------------------------------------Ejercicios de impuesto modificado---------------------------------------------------------------------------------------------------------

def tax(bill): #def es el comando para definir una función
    """ Añade 8% de impuestos a la cuenta de un restaurante."""
    bill *= 1.08
    print "With tax: %f" % bill
    return bill

def tip(bill):
    """Añade 15% de propina a la cuenta del restaurante."""
    bill *= 1.15
    print "With tip: %f" % bill
    return bill

meal_cost = 100
meal_with_tax = tax(meal_cost)
meal_with_tip = tip(meal_with_tax)

    #-----------------------------------------------Función para calcular el cuadrado de un número--------------------------------------------------------------------------------------------------------------------------------------

def square(n):
    """Devuelve el cuadrado de un número"""
    squared = n ** 2
    print "%d squared is %d." % (n, squared)
    return squared

square(10)# en este caso estamos calculando el cuadrado del número 10
    #------------------------------------------------Función algo mas compleja a la de arriba-------------------------------------------------------------------------------------------------------------

def power(base, exponent):  # estos son los parametros!
    result = base ** exponent
    print "%d to the power of %d is %d." % (base, exponent, result)

power(37, 4)  # esto los argumentos!
    #-----------------------------------------------Función llamando otra función----------------------------------------------------------------------------------------

def one_good_turn(n):
    return n + 1

def deserves_another(n):
    return one_good_turn(n) + 2

    #-----------------------------------------------Ejercicios para calcular el cubo de un argumento(number)-----------------------------------------------------------------------------------

"""Primero, defina una función llamada cubo que toma un argumento llamado número. ¡No olvides los paréntesis y el dos puntos!
Haga que esa función devuelva el cubo de ese número (es decir, ese número multiplicado por sí mismo y multiplicado por sí mismo una vez más).
Definir una segunda función llamada por_tres que toma un argumento llamado número.
Si ese número es divisible por 3, por_tres debe llamar al cubo (número) y devolver su resultado. De lo contrario, by_three debería devolver False."""

def cube(number):
    return number ** 3
def by_three(number):
    if number % 3 == 0:
        print "number is divisible by 3"
        return cube(number)
    else:
        return False

    #------------------------------------------------Importacion de módulos---------------------------------------------------------------------------------------

import math # Importa el módulo math
everything = dir(math) # Establece todo en una lista de lo que tiene el módulo math
print everything # imprime todo

    #------------------------------------------------Importacion de módulos otro ejemplo---------------------------------------------------------------------------------------

from math import sqrt
print math.sqrt(13689)

    #-------------------------------------------------EJERCICIO PARA PRACTICAR Y ENTENDER--------------------------------------------------------------------------------------

def biggest_number(*args):
    print max(args)
    return max(args)

def smallest_number(*args):
    print min(args)
    return min(args)

def distance_from_zero(arg):
    print abs(arg)
    return abs(arg)

biggest_number(-10, -5, 5, 10)
smallest_number(-10, -5, 5, 10)
distance_from_zero(-10)

    #-------------------------------------------------Algunas funciones muy sencillas--------------------------------------------------------------------------------------

maximun = max(1,2,3,8)

print maximun #esta impresión return 8 que es el maximo

minimum = min(0,2,1,5,8)

print minimum #esta impresión return 0 que es el minimo

absolute = abs(-42)

print absolute #esta impresión return 42 que es el valor absoluto, siempre devolvera un numero positivo.

    #-------------------------------------------------Ejercicio sencillo para practicas--------------------------------------------------------------------------------------

"""Primero, defina una función, shut_down, que toma un argumento s. ¡No olvide los paréntesis o el colon!
Entonces, si la función shut_down recibe una s igual a "yes", debería devolver "Shutting down".
Alternativamente, elif s es igual a "no", entonces la función debería regresar "Shutdown aborted".
Finalmente, si shut_down recibe algo más que esas entradas, la función debería devolver "lo siento"."""



def shut_down(s):
    if s == "yes":
        return "Shutting down"
    elif s == "no":
        return "Shutdown aborted"
    else:
        return "Sorry"

    #-------------------------------------------------Ejercicio sencillo para practicas--------------------------------------------------------------------------------------

"""Primero, defina una función llamada distance_from_zero, con un argumento (seleccione cualquier nombre de argumento que desee).
Si el tipo de argumento es int o float, la función debería devolver el valor absoluto de la entrada de función.
De lo contrario, la función debería devolver "Nope"."""     

def distance_from_zero(n):
    if type(n) == int or type(n) == float:
        return abs(n)
    else:
        return "Nope"

    #--------------------------------------------------EJercicio de costo de-------------------------------------------------------------------------------------

#costo del hotel

def hotel_cost(nights):
    # if the nights cost $140
        return 140 * nights

#costo del voleto de viaje      

def plane_ride_cost(city):
    if city == "Charlotte":
        return 183
    elif city == "Tampa": #-----------
        return 220                   #|
    elif city == "Pittsburgh":       #|---- puedo usar varios 'elif' para condicionar varias cosas.
        return 222                   #|     Recuerde que una declaración elif sólo se marca 
    elif city == "Los Angeles":#------          si todas las anteriores fallan.
        return 475

#costo del transporte
def rental_car_cost(days):
    rent = 40 * days #costo diario $40
    if days >= 7: # =+ de 7 dias -$50
        rent -= 50
    elif days >= 3: # =+ de 3 dias -$20
        rent -= 20
    return rent
def trip_cost(city, days):
    return rental_car_cost(days) + hotel_cost(days) + plane_ride_cost(city) #suma de todas las funciones

#costo de gastos total
def trip_cost(city, days, spending_money):
    return spending_money + rental_car_cost(days) + hotel_cost(days) + plane_ride_cost(city)

print trip_cost("Los Angeles", 5, 600) #puedo imprimir los valores que desee segun lo establecido en las funciones.

    #---------------------------------------------------------------------------------------------------------------------------------------

#*****************************************************************************************************************************************************************************************




#*****************************************lo aprendido en la quinta leccion**************************************************************************************************************

    #-----------------------------------------------------Listas--------------------------------------------------------------------------------------------------------------------------------

zoo_animals = ["pangolin", "cassowary", "sloth", "wolf" ];

if len(zoo_animals) > 3:
    print "The first animal at the zoo is the " + zoo_animals[0]
    print "The second animal at the zoo is the " + zoo_animals[1]
    print "The third animal at the zoo is the " + zoo_animals[2]
    print "The fourth animal at the zoo is the " + zoo_animals[3]

    #------------------------------------------Ejemplos de indice sencillos para usar-------------------------------------------------------------------------------------------------------------------------------------------

numbers = [5, 6, 7, 8]

print "Adding the numbers at indices 0 and 2..."
print numbers[0] + numbers[2] # imprime 12 que es 5 + 7
print "Adding the numbers at indices 1 and 3..."
print numbers[1] + numbers[3] # imprime 14 que es 6 + 8

    #------------------------------------------Ejemplo de .append()-------------------------------------------------------------------------------------------------------------------------------------------

suitcase = [] 
suitcase.append("sunglasses")
suitcase.append("banio")# /----> .append es para agregar informacion a lo que ya existe
suitcase.append("traje")
suitcase.append("lentes")

list_length = len(suitcase) # Set this to the length of suitcase

print "There are %d items in the suitcase." % (list_length)
print suitcase

    #---------------------------------------------llamar index como slice desde una lista----------------------------------------------------------------------------------------------------------------------------------------

suitcase = ["sunglasses", "hat", "passport", "laptop", "suit", "shoes"]

# The first and second items (index zero and one)
first = suitcase[0:2]#---------------------------
                                              #  |
# Third and fourth items (index two and three)#  |
middle = suitcase[2:4]                        #  |------- slice: me corta desde el valor indicado hasta el otro indicado
                                              #  |              sin tomar el ultimo valor que se le dio
# The last two items (index four and five)    #  |
last =  suitcase[4:6] #--------------------------

    #---------------------------------------buscar el index y asignarle a esa busqueda un nuevo valor----------------------------------------------------------------------------------------------------------------------------------------------

animals = ["aardvark", "badger", "duck", "emu", "fennec fox"]
duck_index =  animals.index("duck")# Use index() to find "duck"
animals.insert(duck_index, "cobra")
# Your code here!
print animals # Observe what prints after the insert operation

    #----------------------------------------realizar una accion con un bucle o loop---------------------------------------------------------------------------------------------------------------------------------------------

my_list = [1,9,3,8,5,7]

for number in my_list:
    print 2 * number

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

"""Escribe un bucle for-loop que itera sobre start_list y. append ()s cada número cuadrado (x ** 2) a square_list.

    Entonces clasifica square_list!"""

start_list = [5, 3, 1, 2, 4]
square_list = []
for x in start_list:
    square_list.append(x**2)#----> .append aqui esta agregando a una variable nueva el cuadrado de los numeros de la variable start_list
    square_list.sort()
print square_list

    #-------------------------------------Para mostrar las llaves de cada elemento------------------------------------------------------------------------------------------------------------------------------------------------

residents = {'Puffin' : 104, 'Sloth' : 105, 'Burmese Python' : 106}

print residents['Puffin']
print residents['Sloth']
print residents['Burmese Python']

    #-------------------------------------Agregar llaves a una variable------------------------------------------------------------------------------------------------------------------------------------------------

menu = {} # Diccionario vacío
menu['Chicken Alfredo'] = 14.50
print menu['Chicken Alfredo']

menu['proteico'] = 12.50
menu['sopa'] = 8.50
menu['ensalada'] = 4.20


print "There are " + str(len(menu)) + " items on the menu."
print menu

    #----------------------------------------Eliminar y cambiar valores de un diccionario---------------------------------------------------------------------------------------------------------------------------------------------

# llave - nombre del animal : valor - localidad 
zoo_animals = { 'Unicorn' : 'Cotton Candy House',
'Sloth' : 'Rainforest Exhibit',
'Bengal Tiger' : 'Jungle House',
'Atlantic Puffin' : 'Arctic Exhibit',
'Rockhopper Penguin' : 'Arctic Exhibit'}
#arriba tenemos la informacion del diccionario.

# eliminamos Unicorn, Sloth y Bengal Tiger
del zoo_animals['Unicorn']
del zoo_animals['Sloth']
del zoo_animals['Bengal Tiger']

#modificamos el valor de Rockhopper Penguin
zoo_animals['Rockhopper Penguin'] = 'Exhibicion desde el artico'

print zoo_animals

    #---------------------------------------Remover valores de un diccionario----------------------------------------------------------------------------------------------------------------------------------------------

backpack = ['xylophone', 'dagger', 'tent', 'bread loaf']
backpack.remove("dagger")

    #-----------------------------------------Entendido--------------------------------------------------------------------------------------------------------------------------------------------

inventory = {
    'gold' : 500,
    'pouch' : ['flint', 'twine', 'gemstone'], # Assigned a new list to 'pouch' key
    'backpack' : ['xylophone','dagger', 'bedroll','bread loaf']
    }

inventory['burlap bag'] = ['apple', 'small ruby', 'three-toed sloth']

inventory['pouch'].sort() 

inventory['pocket'] = ['seashell', 'strange berry', 'lint']
inventory["backpack"].sort()
inventory['backpack'].remove('dagger')
inventory['gold'] += 50 

print inventory

    #------------------------------------------Para usar key-------------------------------------------------------------------------------------------------------------------------------------------

webster = {
    "Aardvark" : "A star of a popular children's cartoon show.",
    "Baa" : "The sound a goat makes.",
    "Carpet": "Goes on the floor.",
    "Dab": "A small amount."
    }

for key in webster:
    print webster[key]

    #------------------------------------------Imprimir un bucle de numeros par-------------------------------------------------------------------------------------------------------------------------------------------

a = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
for number in a:
    if number % 2 == 0:
        print number

#-------------------------------------------Encontrar un dato en especifico------------------------------------------------------------------------------------------------------------------------------------------

def fizz_count(x):
    count = 0
    for item in x:
        if item == "fizz":
            count = count + 1
        return count

print fizz_count(["fizz", 'fizz'])

    #-------------------------------------------Interesante si se desea hacer un acrostico------------------------------------------------------------------------------------------------------------------------------------------

for letter in "Codecademy":
    print letter

# lineas vacias para dejar espacios entre las impresiones
print
print

word = "Programming is fun!"

for letter in word:
    # Only print out the letter i
    if letter == "i":
        print letter

    #----------------------------------------Ejercicio para practica---------------------------------------------------------------------------------------------------------------------------------------------

"""Realice un bucle a través de cada clave en los precios.
Al igual que el ejemplo anterior, para cada clave, imprima la clave junto con su información de precios y existencias.
Imprima la respuesta en el siguiente formato EXACTAMENTE:

manzana
precio: 2
Existencias: 0"""

prices = {
    "banana": 4,
    "apple": 2,
    "orange": 1.5,
    "pear": 3
    }
stock = {
    "banana": 6,
    "apple": 0,
    "orange": 32,
    "pear": 15
    }
#mostramos los datos en un formato agradable
for key in prices:
    print key
    print "price: %s" % prices[key]
    print "stock: %s" % stock[key]

#sumemos los totales de prices por stock    
total = 0
for item in prices and stock:
    total += prices[item] * stock[item]
print "este es el total de ganancia: "
print total

#definimos una funcion que sume el precio del articulo al total
def compute_bill(food):
    total = 0
    for key in food:
        if stock[key] > 0: #---> para no sumar  los valores menore que 1
            total += prices[key]
            stock[key] -= 1
    return total

    #-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

#*****************************************************************************************************************************************************************************************

#*****************************************lo aprendido en la sexta leccion**************************************************************************************************************

    #------------------------------------Ejercicio completo de estudiantes y sus promedios-----------------------------------------------------------------------------------------------------------------------------

#datos de los estudiantes
lloyd = {
    "name": "Lloyd",
    "homework": [90.0, 97.0, 75.0, 92.0],
    "quizzes": [88.0, 40.0, 94.0],
    "tests": [75.0, 90.0]
}
alice = {
    "name": "Alice",
    "homework": [100.0, 92.0, 98.0, 100.0],
    "quizzes": [82.0, 83.0, 91.0],
    "tests": [89.0, 97.0]
}
tyler = {
    "name": "Tyler",
    "homework": [0.0, 87.0, 75.0, 22.0],
    "quizzes": [0.0, 75.0, 78.0],
    "tests": [100.0, 100.0]
}
students = [lloyd, alice, tyler]
for student in students:    
    print student["name"]
    print student["homework"]
    print student["quizzes"]
    print student["tests"]
#funcion para el promedio de las notas
def average(numbers):
    total = sum(numbers) 
    total = float(total) / len(numbers)
    return total
#funcion para calcular el promedio de los estudiantes
def get_average(student):
    return average(student["homework"]) * .10 + average(student["quizzes"]) * .30 + average(student["tests"]) * .60
#funcion para mostrar la nota que sacon un estudiante dependiendo de su ponderacion
def get_letter_grade(score):
    if score >= 90:
        return "A"
    elif score >= 80 and score < 90:
        return "B"
    elif score >= 70 and score < 80:
        return "C"
    elif score >= 60 and score < 70:
        return "D"
    elif score < 60:
        return "F"
    return score
print get_letter_grade(get_average(lloyd))

#funcion para calcular el promedio de toda la clase
def get_class_average(student):
    results = []
    for each in student:
        result = get_average(each)
        results.append(result)
        avg = average(results)
    return avg
students = [alice, lloyd, tyler]
print "Este es el promedio de la clase en numeros: %s" % get_class_average(students)
print "Este es el promedio de la clase en letra: %s" % get_letter_grade(get_class_average(students))#---> aqui se muestra el promedio de la clase
#************************************************************************************************************************************************************************************************